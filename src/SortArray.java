import org.junit.Assert;

public class SortArray {
	
	// some static stuff to keep track of what we're doing.
	public static int round = 0;
	public static int num_test = 0;
	public static int num_pass = 0;
	public static int num_fail = 0;
	
	public static void main(String[] args) {
		
		System.out.println("--- Round " + String.valueOf(++round) + " ---");
		// Creating an initial array of random order to work with.
		int[] unsorted = {-15, 5, -1, 40, -6, 29, 5, 11, 0, -1, 14, 32, 10};
		
		// Calls to the sorting methods.
		int[] sorted_desc = descArray(unsorted);
		int[] sorted_asc = ascArray(unsorted);

		// Printing and testing.
		System.out.println("Original Order:");
		printArray(unsorted);
		
		System.out.println("Ascending Order:");
		printArray(sorted_asc);
		int[] exp_asc = {-15, -6, -1, -1, 0, 5, 5, 10, 11, 14, 29, 32, 40};
		runTest(exp_asc, sorted_asc);
		
		System.out.println("Descending Order:");
		printArray(sorted_desc);		
		int[] exp_desc = {40, 32, 29, 14, 11, 10, 5, 5, 0, -1, -1, -6, -15};
		runTest(exp_desc, sorted_desc);
		
		/////////////
		
		System.out.println("--- Round " + String.valueOf(++round) + " ---");
		// Creating an initial array of random order to work with.
		int[] unsorted_2 = {10, 2, -4, 5, 18, 19, -3, -10};
		
		// Calls to the sorting methods.
		int[] sorted_desc_2 = descArray(unsorted_2);
		int[] sorted_asc_2 = ascArray(unsorted_2);

		// Printing and testing.
		System.out.println("Original Order:");
		printArray(unsorted_2);
		
		System.out.println("Ascending Order:");
		printArray(sorted_asc_2);
		int[] exp_asc_2 = {-10, -4, -3, 2, 5, 10, 18, 19};
		runTest(exp_asc_2, sorted_asc_2);
		
		System.out.println("Descending Order:");
		printArray(sorted_desc_2);		
		int[] exp_desc_2 = {19, 18, 10, 5, 2, -3, -4, -10};
		runTest(exp_desc_2, sorted_desc_2);

		/////////////
		
		System.out.println("--- Round " + String.valueOf(++round) + " ---");
		// Creating an initial array of random order to work with.
		int[] unsorted_3 = {-3, 20, 14, -1, 0, 4, 7, -15};
		
		// Calls to the sorting methods.
		int[] sorted_desc_3 = descArray(unsorted_3);
		int[] sorted_asc_3 = ascArray(unsorted_3);

		// Printing and testing.
		System.out.println("Original Order:");
		printArray(unsorted_3);
		
		System.out.println("Ascending Order:");
		printArray(sorted_asc_3);
		int[] exp_asc_3 = {-15, -3, -1, 0, 4, 7, 14, 20};
		runTest(exp_asc_3, sorted_asc_3);
		
		System.out.println("Descending Order:");
		printArray(sorted_desc_3);		
		int[] exp_desc_3 = {20, 14, 7, 4, 0, -1, -3, -15};
		runTest(exp_desc_3, sorted_desc_3);
		

		
		// results of the testing.
		results();
    }
    
    // Method for sorting by descending order.
    public static int[] descArray(int[] a) {
		
		// General 'ceiling' to be used through the
		// method, keeping us exception free.
		int a_l = a.length;
		// An array of the length of the ceiling to store the results.
		int[] ret = new int[a_l];
		// A boolean array used as flags for which indexes from the input
		// array have been used, because we cannot use an index twice.
		// All values are all set to false by default.
		boolean[] used = new boolean[a_l];
		
		// Creating a value for the temporary with the lowest value possible
		// from the array that was passed in - keeps a general boundary set.
		int t_init = 0;
		for (int i = 0; i < a_l; i++) {
			if (a[i] < t_init) { t_init = a[i]; }
		}
		
		for (int i = 0; i < a_l; i++) {
			
			int t = t_init;
			int index = 0;
			for (int j = 0; j < a_l; j++) {
				
				// If the current index has been used, we move to the next.
				if (used[j]) { continue; }
				
				// If the current index is greater than the temporary value,
				// the new temporary value is the current index of the array, 
				// and the index we will potentially set to the return array
				// is the current value of j in the loop.
				if (a[j] >= t) {
					t = a[j];
					index = j;
				}
				
			}
			
			// Marking the index as used, and transfering the value of the
			// index of the input array to the return array.
			used[index] = true;
			ret[i] = a[index];
		}
		
		return ret;
	}
	
	// Method for sorting by ascending order.
	public static int[] ascArray(int[] a) {
		// General 'ceiling' to be used through the
		// method, keeping us exception free.
		int a_l = a.length;
		// An array of the length of the ceiling to store the results.
		int[] ret = new int[a_l];
		// A boolean array used as flags for which indexes from the input
		// array have been used, because we cannot use an index twice.
		// All values are all set to false by default.
		boolean[] used = new boolean[a_l];
		
		// Creating a value for the temporary with the highest value possible
		// from the array that was passed in - keeps a general boundary set.
		int t_init = 0;
		for (int i = 0; i < a_l; i++) {
			if (a[i] > t_init) { t_init = a[i]; }
		}
		
		for (int i = 0; i < a_l; i++) {
			
			int t = t_init;
			int index = 0;
			for (int j = 0; j < a_l; j++) {
				
				// If the current index has been used, we move to the next.
				if (used[j]) { continue; }
				
				// If the current index is less than the temporary value,
				// the new temporary value is the current index of the array, 
				// and the index we will potentially set to the return array
				// is the current value of j in the loop.
				if (a[j] <= t) {
					t = a[j];
					index = j;
				}
				
			}
			
			// Marking the index as used, and transfering the value of the
			// index of the input array to the return array.
			used[index] = true;
			ret[i] = a[index];
		}
		return ret;
	}
	
	// Just a quick method to print out an array.
	public static void printArray(int[] ar) {

		String s = "";
		for (int i = 0; i < ar.length; i++) {
			if (!s.equals("")) { s += ", "; }
			s += String.valueOf(ar[i]);
		}	
		System.out.println(s);
			
	}
	
	// Test Method.
	public static void runTest(int [] exp_ar, int[] ar) {
		num_test++;
		try {
			Assert.assertArrayEquals(exp_ar, ar);
			System.out.println("Test Passed.");
			num_pass++;
		} catch(Exception e) {
			System.out.println("Test Failed.");
			num_fail++;
		}
	}
	public static void results() {
		System.out.println("--- Results from total of " + num_test + " test(s) in " + round + " round(s) ---");
		System.out.println("Passed: " + num_pass + "\nFailed: " + num_fail);
	}
}
